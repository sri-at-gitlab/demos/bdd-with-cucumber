# BDD with Cucumber

This sample projects shows BDD (Cucumber JS) integration with GitLab.

## Dependencies

- `cucumber`
- `cucumber-junit-formatter`

## Successful Tests

The merge request titled [Simple math feature](https://gitlab.com/sri-at-gitlab/demos/bdd-with-cucumber/-/merge_requests/1) includes the `features` and `steps` alongwith the Pipeline definition to execute the tests.

As [all tests are passing](https://gitlab.com/sri-at-gitlab/demos/bdd-with-cucumber/-/pipelines/189109407/test_report) the merge request contains a minimal mention.

## Failing Tests

However in the merge request titled [make a test fail](https://gitlab.com/sri-at-gitlab/demos/bdd-with-cucumber/-/merge_requests/2) a [breaking change](https://gitlab.com/sri-at-gitlab/demos/bdd-with-cucumber/-/merge_requests/2/diffs#7c50cf44fb1619c353d1fd63b97af5f483c5ff6d_13_13) is introduced. This leads to 4 test cases failing out of a total of 11.

The four failing tests are highlighted in the merge request and additional details are available on click of each failing case and in the [full test report](https://gitlab.com/sri-at-gitlab/demos/bdd-with-cucumber/-/pipelines/189110268/test_report).
